FROM ubuntu:22.04

ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

# install bootstrap tools
RUN --mount=type=cache,target=/var/cache/apt \
    --mount=type=cache,target=/var/lib/apt/lists \
    apt-get update && \
    echo 'Etc/UTC' > /etc/timezone && \
    apt-get install --no-install-recommends -y \
    tzdata git git-lfs bc bison flex gcc g++ libssl-dev make libc6-dev libncurses5-dev \
    crossbuild-essential-arm64 ccache curl wget ca-certificates openssh-client \
    bash-completion nano vim zip unzip 

RUN useradd ubuntu && \
    usermod -aG ubuntu ubuntu && \
    echo 'ubuntu:100000:65536' >> /etc/subuid && \
    echo 'ubuntu:100000:65536' >> /etc/subgid && \
    mkdir /home/ubuntu

USER ubuntu
WORKDIR /home/ubuntu

ENV USE_CCACHE=1
ENV CCACHE_EXEC=/usr/bin/ccache
