#!/bin/bash

export DOCKER_BUILDKIT=1

docker build -t aarch64-cross-compile-env:latest -f ./Dockerfile .
docker run --rm -v .:/home/ubuntu -it aarch64-cross-compile-env:latest /bin/bash
